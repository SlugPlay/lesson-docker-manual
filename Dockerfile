FROM python:3-slim
ENV PYTHONUNBUFFERED 1

WORKDIR /app
COPY . /app
RUN pip install -r requirements.txt && \
	chmod +x /app/init.sh

CMD sh init.sh

#ENTRYPOINT  /app/init.sh
